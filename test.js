// Dữ liệu sinh viên mẫu để kiểm thử
let students = [
  { id: "20201245", name: "Mai Hồng Thắm", age: 20, grade: "7.5" },
  { id: "20201452", name: "Nguyễn Văn Lợi", age: 21, grade: "9.5" },
  { id: "20201745", name: "Nguyễn Thái", age: 22, grade: "9" },
  { id: "20224587", name: "Lê Khắc Kiên", age: 21, grade: "8.5" },
  { id: "20221464", name: "Trịnh Thu Phương", age: 23, grade: "7.5" },
  { id: "520214578", name: "Lê Lưu Ly", age: 20, grade: "5" },
  { id: "20203541", name: "Mai Văn Phúc", age: 20, grade: "5.5" },
  { id: "20214789", name: "Lưu Văn Phong", age: 24, grade: "6.5" },
  // Thêm nhiều dữ liệu sinh viên khác nếu cần
];

// Hàm thêm sinh viên mới
function addStudent() {
  var inputID = document.getElementById("inputID").value;
  var inputName = document.getElementById("inputName").value;
  var inputAge = document.getElementById("inputAge").value;
  var inputGrade = document.getElementById("inputGrade").value;

  // Kiểm tra xem tất cả các trường có được điền đầy đủ hay không
  if (inputID && inputName && inputAge && inputGrade) {
    //Thêm điều kiện để check MSSV đã tồn tại chưa, nếu tồn tại thì sẽ báo lỗi
    if (!students.find((x) => x.id === inputID)) {
      const newStudent = {
        id: inputID,
        name: inputName,
        age: parseInt(inputAge),
        grade: inputGrade,
      };
      students.push(newStudent);
      renderStudents(students);
    } else {
      alert("MSSV đã được sử dụng");
    }
  } else {
    alert("Vui lòng điền đầy đủ thông tin sinh viên");
  }
}

// Hàm sửa sinh viên dựa trên ID
// Hàm sửa sinh viên dựa trên ID
function editStudent(studentId) {
  const index = students.findIndex((student) => student.id === studentId);

  if (index !== -1) {
    const editedName = prompt("Nhập tên mới:", students[index].name);
    const editedAge = prompt("Nhập tuổi mới:", students[index].age);
    const editedGrade = prompt("Nhập điểm mới:", students[index].grade);

    // Kiểm tra xem các trường nhập liệu có hợp lệ hay không
    if (editedName !== null && editedAge !== null && editedGrade !== null) {
      students[index].name = editedName || students[index].name;
      students[index].age = !isNaN(editedAge)
        ? parseInt(editedAge)
        : students[index].age;
      students[index].grade = editedGrade || students[index].grade;

      // Cập nhật lại dòng trong bảng
      const row = document.getElementById(row - studentId);
      row.innerHTML = `
                <td>${students[index].id}</td>
                <td>${students[index].name}</td>
                <td>${students[index].age}</td>
                <td>${students[index].grade}</td>
                <td><button onclick="editStudent('${students[index].id}')">Sửa</button></td>
            `;

      // Giữ lại thông tin trên biểu mẫu
      //   document.getElementById("inputID").value = students[index].id;
      document.getElementById("inputName").value = students[index].name;
      document.getElementById("inputAge").value = students[index].age;
      document.getElementById("inputGrade").value = students[index].grade;
    }
  }
}

// Hàm hiển thị danh sách sinh viên
function renderStudents(studentArray) {
  const studentListContainer = document.getElementById("studentList");
  studentListContainer.innerHTML = ""; // Xóa nội dung cũ của tbody để làm mới danh sách

  studentArray.forEach((student) => {
    const row = document.createElement("tr");
    row.innerHTML = `
            <td>${student.id}</td>
            <td>${student.name}</td>
            <td>${student.age}</td>
            <td>${student.grade}</td>
            <button onclick="editStudent('${student.id}')" class="btn btn-warning mr-3">Sửa</button>
                <button onclick="deleteStudent('${student.id}')" class="btn btn-danger">Xóa</button>
           
        `;
    studentListContainer.appendChild(row);
  });
}

// Hàm tìm kiếm sinh viên dựa trên tên và điểm đã chọn
function searchStudents() {
  const searchInput = document
    .getElementById("searchInput")
    .value.toLowerCase();
  const filterSelect = document.getElementById("filterSelect");
  const selectedGrade = filterSelect.options[filterSelect.selectedIndex].value;

  const filteredStudents = students.filter((student) => {
    const nameMatch = student.name.toLowerCase().includes(searchInput);
    const gradeMatch =
      selectedGrade === "all" || student.grade === selectedGrade;
    return nameMatch && gradeMatch;
  });

  renderStudents(filteredStudents);
}

// Hàm sắp xếp mảng con theo tên
function sortSubsetByName(subset) {
  return subset.sort((a, b) => a.name.localeCompare(b.name));
}

// Hàm sắp xếp sinh viên (theo tên) sau khi lọc
function sortStudents() {
  const filterSelect = document.getElementById("filterSelect");
  const selectedGrade = filterSelect.options[filterSelect.selectedIndex].value;

  // Lọc sinh viên theo điều kiện đã chọn
  const filteredStudents = students.filter(
    (student) => selectedGrade === "all" || student.grade === selectedGrade
  );

  // Sắp xếp mảng con sau khi lọc
  const sortedSubset = sortSubsetByName(filteredStudents);

  // Hiển thị danh sách sinh viên đã sắp xếp
  renderStudents(sortedSubset);
}

// Hàm xóa sinh viên dựa trên ID
function deleteStudent(studentId) {
  students = students.filter((student) => student.id !== studentId);
  renderStudents(students);
}

// Hàm lọc sinh viên dựa trên điểm đã chọn
function filterStudents() {
  const filterSelect = document.getElementById("filterSelect");
  const selectedGrade = filterSelect.options[filterSelect.selectedIndex].value;

  const filteredStudents = students.filter(
    (student) => selectedGrade === "all" || student.grade === selectedGrade
  );

  renderStudents(filteredStudents);
}

// Hiển thị sinh viên ban đầu khi trang được tải
renderStudents(students);
