var mysql      = require('mysql2');
var connection = mysql.createConnection({
    host : 'localhost',
    database : 'sinhvien',
    user : 'root',
    password : 'root123',
})
module.exports.connectDb = () => {
    return new Promise((resolve, reject) => {
        connection.connect((err) => {
            if(err) {
                console.error('Error connectiong: ' + err.stack);
                return;
            }
            console.log('Connected as id' + connection.threadId);
        })
    })
};

// connection.query('SELECT * FROM sinhvien', function (error, result, fields) {
//     if(error)
//         throw error;

//     result.forEach(result => {
//         console.log(result);
//     });
// });

module.exports.closeDb = (con) => {
    con.destroy();
}

