
const express = require('express');
const app = express();
// const bodyParser = require('body-parser');
const port = process.env.PORT || 5500;
const apiPath = '/api/';
const apiRouter = require('./routes/items.route')
const db = require('./next.js');

// const multer = require('multer');

app.use(express.json());
app.use(express.urlencoded());

// website
app.use(express.static('/'));


//Hạnh
//delete data by id
app.get('/delete/:id', function(req, res){
	var itemId = req.params.id;
	db.connectDB()
			.then((connection) => {
				connection.query(
					'DELETE FROM sinhvien.sinhvien Where mssv = ?', 
					[itemId] ,
					function (err, data, fields) {
						console.log('data',data);
						db.closeDB(connection);
						return res.status(200).json({ result: `Đã xóa data` });
					}
				);
			})
			.catch((error) => {
				console.log('Db not connected successfully', error);
				return res
					.status(200)
					.json({ result: `Không thể kết nối Database` });
			});
})


//edit data by id
app.post('/edit/:id',function(req, res){

	var itemId = req.params.mssv
	// else{
	// 	image = req.body.oldImg;
	// }

	const ID = req.body.inputID;
	const Name = req.body.inputName;
	const Age = req.body.inputAge;
	const Grade =req.body.inputGrade;

	// const image = req.file.filename;
	const color = req.body.color;


	db.connectDB()
		.then((connection) => {
			console.log('connected successfully');
			connection.query(  
				// missing img
				`UPDATE sinhvien.sinhvien SET inputID = '${ID}', inputName = '${Name}', inputAge = '${Age}', inputGrade = ${Grade} WHERE id = ?`
				,[itemId] ,
				function (err, data, fields) {
					db.closeDB(connection);
					return res.status(200).json({ result: `đã sửa item ${image}` });
				}
			);
		})
		.catch((error) => {
			console.log('Db not connected successfully', error);
			return res
				.status(200)
				.json({ result: `Không thể kết nối Database` });
		});
})

//Hưng
// routers
app.use(apiPath + 'items', apiRouter);
// app.use(apiPath + 'products', require('./routes/products.route'));
// app.use(apiPath + 'upload', require('./routes/upload.route'));

app.listen(port, function () {
	const host = 'localhost';
	console.log('Example app listening at http://%s:%s', host, port);
});